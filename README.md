# README #

### What is this repository for? ###

This repo is for a tool which helps to find square triangle numbers, as well as looking into adding pentagonal numbers into the mix also. This was created as a response to Matthew Parker's video on the topic here: https://www.youtube.com/watch?v=Gh8h8MJFFdI

### How do I get set up? ###

To get setup, you can either download the source code, tweak it as needed, compile and run or there are release versions available [here](https://bitbucket.org/pingu2k4/trisquares2/downloads/setup.exe).

### What else is there? ###

There are additional files, such as numbers already generated with this tool available in the [downloads section](https://bitbucket.org/pingu2k4/trisquares2/downloads).

### Screenshots ###

![1.png](https://bitbucket.org/repo/yBo5gx/images/956946330-1.png) ![2.png](https://bitbucket.org/repo/yBo5gx/images/1694704756-2.png) ![3.png](https://bitbucket.org/repo/yBo5gx/images/3757454291-3.png)

### Who do I talk to? ###

Repo owned by Matthew Parker (not the one from the video). Contact "pingu2k4".
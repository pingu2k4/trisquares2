﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using TRISQUARES2.Commands;
using TRISQUARES2.Models;
using TRISQUARES2.Views;

namespace TRISQUARES2.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            SettingsUpgrade();
            SetBGOpacity();

            FileLocation = Properties.Settings.Default.LastLocation;

            BGWorker = new BackgroundWorker();
            BGWorker.WorkerReportsProgress = true;
            BGWorker.WorkerSupportsCancellation = true;
            BGWorker.DoWork += BGWorker_DoWork;
            BGWorker.ProgressChanged += BGWorker_ProgressChanged;
            BGWorker.RunWorkerCompleted += BGWorker_RunWorkerCompleted;

            BGWorker2 = new BackgroundWorker();
            BGWorker2.WorkerReportsProgress = true;
            BGWorker2.WorkerSupportsCancellation = true;
            BGWorker2.DoWork += BGWorker2_DoWork;
            BGWorker2.ProgressChanged += BGWorker2_ProgressChanged;
            BGWorker2.RunWorkerCompleted += BGWorker2_RunWorkerCompleted;

            BGWorker3 = new BackgroundWorker();
            BGWorker3.WorkerReportsProgress = true;
            BGWorker3.WorkerSupportsCancellation = true;
            BGWorker3.DoWork += BGWorker3_DoWork;
            BGWorker3.ProgressChanged += BGWorker3_ProgressChanged;
            BGWorker3.RunWorkerCompleted += BGWorker3_RunWorkerCompleted;

            BGWorker4 = new BackgroundWorker();
            BGWorker4.WorkerReportsProgress = true;
            BGWorker4.WorkerSupportsCancellation = true;
            BGWorker4.DoWork += BGWorker4_DoWork;
            BGWorker4.ProgressChanged += BGWorker4_ProgressChanged;
            BGWorker4.RunWorkerCompleted += BGWorker4_RunWorkerCompleted;

            BGWorker5 = new BackgroundWorker();
            BGWorker5.WorkerReportsProgress = true;
            BGWorker5.WorkerSupportsCancellation = true;
            BGWorker5.DoWork += BGWorker5_DoWork;
            BGWorker5.ProgressChanged += BGWorker5_ProgressChanged;
            BGWorker5.RunWorkerCompleted += BGWorker5_RunWorkerCompleted;

            BGWorker6 = new BackgroundWorker();
            BGWorker6.WorkerReportsProgress = true;
            BGWorker6.WorkerSupportsCancellation = true;
            BGWorker6.DoWork += BGWorker6_DoWork;
            BGWorker6.ProgressChanged += BGWorker6_ProgressChanged;
            BGWorker6.RunWorkerCompleted += BGWorker6_RunWorkerCompleted;
        }

        private void BGWorker6_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WorkerRunning = false;
            WorkerStopped = true;
            Int2ErrorMessage = "COMPLETE!";
        }

        private void BGWorker6_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BigInteger State = (BigInteger)e.UserState;

            Int2StatusMessage = State.ToString("0,0") + " Tested.\n" + Interesting2Number.ToString("0,0") + " has " + Interesting2NumberPolygons + " Polygonal roots.";
        }

        private void BGWorker6_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            Interesting2Number = 0;
            Interesting2NumberPolygons = 0;
            CurrentBases = new List<BigInteger>();
            CurrentNexts = new List<BigInteger>();

            BigInteger i = Int2Start;
            if (NewFile)
            {
                using (StreamWriter ResultsWriter = File.CreateText(@FileLocation + "Interesting2Numbers.dat"))
                {
                    ResultsWriter.WriteLine("Number,Count,Sides 1,Base 1,Sides 2,Base 2,Sides 3,Base 3,Sides 4,Base 4,Sides 5,Base 5,Sides 6,Base 6,Sides 7,Base 7,Sides 8,Base 8,Sides 9,Base 9,Sides 10,Base 10,Sides 11,Base 11,Sides 12,Base 12,Sides 13,Base 13,Sides 14,Base 14,Sides 15,Base 15,Sides 16,Base 16,Sides 17,Base 17,Sides 18,Base 18,Sides 19,Base 19,Sides 20,Base 20");
                    if (i <= 0)
                    {
                        ResultsWriter.WriteLine("0,0,0,0,0,0,0,0,0,0,0");
                        i = 1;
                    }
                    if (i == 1)
                    {
                        ResultsWriter.WriteLine("1,0,1,1,1,1,1,1,1,1,1,1");
                        i++;
                    }
                }
            }
            else
            {
                if (!File.Exists(@FileLocation + "Interesting2Numbers.dat"))
                {
                    Int2ErrorMessage = "InterestingNumbers.dat does not exist at the given location! Instead, check the Create File option.";
                    return;
                }
                using (StreamWriter ResultsWriter = File.AppendText(@FileLocation + "Interesting2Numbers.dat"))
                {
                    if (i <= 0)
                    {
                        ResultsWriter.WriteLine("0,0,0,0,0,0,0,0,0,0,0,0");
                        i = 1;
                    }
                    if (i == 1)
                    {
                        ResultsWriter.WriteLine("1,0,1,1,1,1,1,1,1,1,1,1");
                        i++;
                    }
                }
            }

            int j = 0;
            while(true)
            {
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                bool Finished = false;
                int k = 3;
                while (!Finished)
                {
                    if (k >= CurrentBases.Count)
                    {
                        CurrentBases.Add(1);
                        CurrentNexts.Add(1);
                    }
                    else
                    {
                        if(CurrentNexts[k] < i)
                        {
                            CurrentBases[k]++;
                            CurrentNexts[k] = PolygonalNumber(CurrentBases[k], k);
                        }
                        
                        if(CurrentBases[k] <= 3 && CurrentNexts[k] >= i)
                        {
                            Finished = true;
                        }
                        k++;
                    }
                }

                if(CurrentNexts.FindAll(Nexts => Nexts == i).Count >= Int2MinimumSignificance)
                {
                    List<int> Results = Enumerable.Range(0, CurrentNexts.Count)
                                        .Where(ind => CurrentNexts[ind] == i)
                                        .ToList();
                    string Row = "";
                    Row += i + "," + Results.Count;
                    foreach(int Result in Results)
                    {
                        Row += "," + Result + "," + CurrentBases[Result];
                    }

                    using (StreamWriter ResultsWriter = File.AppendText(@FileLocation + "Interesting2Numbers.dat"))
                    {
                        ResultsWriter.WriteLine(Row);
                    }

                    if(Results.Count >= Interesting2NumberPolygons)
                    {
                        Interesting2NumberPolygons = Results.Count;
                        Interesting2Number = i;
                    }
                }

                if (i % 100 == 0)
                {
                    Worker.ReportProgress(0, i);
                    using (StreamWriter DropCatcher = File.CreateText(@FileLocation + "DropCatcher.dat"))
                    {
                        DropCatcher.WriteLine(i);
                    }
                }

                j++;
                i++;
            }
        }

        private void BGWorker5_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WorkerRunning = false;
            WorkerStopped = true;
            IntErrorMessage = "COMPLETE!";
        }

        private void BGWorker5_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BigInteger State = (BigInteger)e.UserState;

            IntStatusMessage = State.ToString("0,0") + " Tested.\n" + InterestingNumber + " has " + InterestingNumberPolygons + " Polygonal Numbers [3-12], shared by " + (InterestingNumberCount - 1) + " others.";
        }

        private void BGWorker5_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            InterestingNumber = 0;
            InterestingNumberCount = 0;
            InterestingNumberPolygons = 0;

            BigInteger i = IntStart;
            if(NewFile)
            {
                using (StreamWriter ResultsWriter = File.CreateText(@FileLocation + "InterestingNumbers.dat"))
                {
                    ResultsWriter.WriteLine("Number,Triangle,Square,Pentagon,Hexagon,Heptagon,Octogon,Nonogon,Decagon,Undecagon,Dodecagon,Count");
                    if (i <= 0)
                    {
                        ResultsWriter.WriteLine("0,0,0,0,0,0,0,0,0,0,0,10");
                        i = 1;
                    }
                    if (i == 1)
                    {
                        ResultsWriter.WriteLine("1,1,1,1,1,1,1,1,1,1,1,10");
                        i++;
                    }
                }
            }
            else
            {
                if(!File.Exists(@FileLocation + "InterestingNumbers.dat"))
                {
                    IntErrorMessage = "InterestingNumbers.dat does not exist at the given location! Instead, check the Create File option.";
                    return;
                }
                using (StreamWriter ResultsWriter = File.AppendText(@FileLocation + "InterestingNumbers.dat"))
                {
                    if (i <= 0)
                    {
                        ResultsWriter.WriteLine("0,0,0,0,0,0,0,0,0,0,0,10");
                        i = 1;
                    }
                    if (i == 1)
                    {
                        ResultsWriter.WriteLine("1,1,1,1,1,1,1,1,1,1,1,10");
                        i++;
                    }
                }
            }

            BigInteger Triangle = 1;
            BigInteger Square = 1;
            BigInteger Pentagon = 1;
            BigInteger Hexagon = 1;
            BigInteger Heptagon = 1;
            BigInteger Octogon = 1;
            BigInteger Nonogon = 1;
            BigInteger Decagon = 1;
            BigInteger Undecagon = 1;
            BigInteger Dodecagon = 1;
            BigInteger NextTriangle = 1;
            BigInteger NextSquare = 1;
            BigInteger NextPentagon = 1;
            BigInteger NextHexagon = 1;
            BigInteger NextHeptagon = 1;
            BigInteger NextOctogon = 1;
            BigInteger NextNonogon = 1;
            BigInteger NextDecagon = 1;
            BigInteger NextUndecagon = 1;
            BigInteger NextDodecagon = 1;
            bool Finished = false;

            while (!Finished)
            {
                Finished = true;
                if(NextTriangle < i)
                {
                    Triangle++;
                    NextTriangle = TriangleNumber(Triangle);
                    Finished = false;
                }
                if (NextSquare < i)
                {
                    Square++;
                    NextSquare = SquareNumber(Square);
                    Finished = false;
                }
                if (NextPentagon < i)
                {
                    Pentagon++;
                    NextPentagon = PentagonNumber(Pentagon);
                    Finished = false;
                }
                if (NextHexagon < i)
                {
                    Hexagon++;
                    NextHexagon = HexagonNumber(Hexagon);
                    Finished = false;
                }
                if (NextHeptagon < i)
                {
                    Heptagon++;
                    NextHeptagon = HeptagonNumber(Heptagon);
                    Finished = false;
                }
                if (NextOctogon < i)
                {
                    Octogon++;
                    NextOctogon = OctogonNumber(Octogon);
                    Finished = false;
                }
                if (NextNonogon < i)
                {
                    Nonogon++;
                    NextNonogon = NonogonNumber(Nonogon);
                    Finished = false;
                }
                if (NextDecagon < i)
                {
                    Decagon++;
                    NextDecagon = DecagonNumber(Decagon);
                    Finished = false;
                }
                if (NextUndecagon < i)
                {
                    Undecagon++;
                    NextUndecagon = UndecagonNumber(Undecagon);
                    Finished = false;
                }
                if (NextDodecagon < i)
                {
                    Dodecagon++;
                    NextDodecagon = DodecagonNumber(Dodecagon);
                    Finished = false;
                }
            }

            int j = 0;
            while (true)
            {
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    try
                    {
                        BigInteger MinValue = MinBIFrom10(NextTriangle, NextSquare, NextPentagon, NextHexagon, NextHeptagon, NextOctogon, NextNonogon, NextDecagon, NextUndecagon, NextDodecagon);

                        if (MinValue > i)
                        {
                            using (StreamWriter ResultsWriter = File.AppendText(@FileLocation + "InterestingNumbers.dat"))
                            {
                                while (MinValue > i)
                                {
                                    ResultsWriter.WriteLine(i + ",-,-,-,-,-,-,-,-,-,-,0");
                                    i++;
                                }
                            }
                        }

                        int Count = 0;
                        string Row = "";

                        Row = i + ",";

                        if(MinValue == NextTriangle)
                        {
                            Row += Triangle + ",";
                            Count++;
                            Triangle++;
                            NextTriangle = TriangleNumber(Triangle);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextSquare)
                        {
                            Row += Square + ",";
                            Count++;
                            Square++;
                            NextSquare = SquareNumber(Square);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextPentagon)
                        {
                            Row += Pentagon + ",";
                            Count++;
                            Pentagon++;
                            NextPentagon = PentagonNumber(Pentagon);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextHexagon)
                        {
                            Row += Hexagon + ",";
                            Count++;
                            Hexagon++;
                            NextHexagon = HexagonNumber(Hexagon);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextHeptagon)
                        {
                            Row += Heptagon + ",";
                            Count++;
                            Heptagon++;
                            NextHeptagon = HeptagonNumber(Heptagon);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextOctogon)
                        {
                            Row += Octogon + ",";
                            Count++;
                            Octogon++;
                            NextOctogon = OctogonNumber(Octogon);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextNonogon)
                        {
                            Row += Nonogon + ",";
                            Count++;
                            Nonogon++;
                            NextNonogon = NonogonNumber(Nonogon);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextDecagon)
                        {
                            Row += Decagon + ",";
                            Count++;
                            Decagon++;
                            NextDecagon = DecagonNumber(Decagon);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextUndecagon)
                        {
                            Row += Undecagon + ",";
                            Count++;
                            Undecagon++;
                            NextUndecagon = UndecagonNumber(Undecagon);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        if (MinValue == NextDodecagon)
                        {
                            Row += Dodecagon + ",";
                            Count++;
                            Dodecagon++;
                            NextDodecagon = DodecagonNumber(Dodecagon);
                        }
                        else
                        {
                            Row += "-,";
                        }

                        Row += Count;

                        using (StreamWriter ResultsWriter = File.AppendText(@FileLocation + "InterestingNumbers.dat"))
                        {
                            ResultsWriter.WriteLine(Row);
                        }

                        if(Count > InterestingNumberPolygons)
                        {
                            InterestingNumber = i;
                            InterestingNumberCount = 1;
                            InterestingNumberPolygons = Count;
                        }
                        else if(Count == InterestingNumberPolygons)
                        {
                            InterestingNumberCount++;
                        }


                        i++;
                    }
                    catch (Exception err)
                    {
                        e.Cancel = true;
                        System.Windows.Forms.MessageBox.Show("Unknown Error Caused: \n" + err.Message);
                        break;
                    }
                }
                if(j % 100 == 0)
                {
                    Worker.ReportProgress(0,i);
                    using (StreamWriter DropCatcher = File.CreateText(@FileLocation + "DropCatcher.dat"))
                    {
                        DropCatcher.WriteLine(i);
                    }
                }
                j++;
            }
        }

        private BigInteger TriangleNumber(BigInteger N)
        {
            return ((N * (N + 1)) / 2);
        }

        private BigInteger SquareNumber(BigInteger N)
        {
            return N * N;
        }

        private BigInteger PentagonNumber(BigInteger N)
        {
            //(3n^2-n)/2
            return (((3 * (N * N)) - N) / 2);
        }

        private BigInteger HexagonNumber(BigInteger N)
        {
            //2n*(2n-1)/2
            return (((N * 2) * (N * 2 - 1)) / 2);
        }

        private BigInteger HeptagonNumber(BigInteger N)
        {
            //5N^2-3n/2
            return (((5 * (N * N)) - (3 * N)) / 2);
        }

        private BigInteger OctogonNumber(BigInteger N)
        {
            //3n^2-2n
            return ((3 * (N * N)) - (2 * N));
        }

        private BigInteger NonogonNumber(BigInteger N)
        {
            //(n(7n-5))/2
            return ((N * (7 * N - 5)) / 2);
        }

        private BigInteger DecagonNumber(BigInteger N)
        {
            //4n^2-3n
            return (4 * (N * N) - 3 * N);
        }

        private BigInteger UndecagonNumber(BigInteger N)
        {
            //(9n^2-7n)/2
            return ((9 * (N * N) - (7 * N)) / 2);
        }

        private BigInteger DodecagonNumber(BigInteger N)
        {
            //(10n^2-8n)/2
            return ((10 * (N * N) - (8 * N)) / 2);
        }

        private BigInteger PolygonalNumber(BigInteger N, BigInteger Sides)
        {
            if (Sides < 3)
            {
                return BigInteger.Zero;
            }
            //TRI: (N^2+N)/2
            else if (Sides == 3)
            {
                return ((N * N + N) / 2);
            }
            //POLY: ((S-2)N^2-(S-4)N)/2
            else
            {
                return (((Sides - 2) * (N * N) - (Sides - 4) * N) / 2);
            }
        }

        private BigInteger MinBIFrom10(BigInteger N0, BigInteger N1, BigInteger N2, BigInteger N3, BigInteger N4, BigInteger N5, BigInteger N6, BigInteger N7, BigInteger N8, BigInteger N9)
        {
            BigInteger Ret = BigInteger.Min(N0, BigInteger.Min(N1, BigInteger.Min(N2, BigInteger.Min(N3, BigInteger.Min(N4, BigInteger.Min(N5, BigInteger.Min(N6, BigInteger.Min(N7, BigInteger.Min(N8, N9)))))))));

            return Ret;
        }

        private void BGWorker4_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WorkerRunning = false;
            WorkerStopped = true;
            GenTSPErrorMessage = "COMPLETE!";
        }

        private void BGWorker4_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Tuple<int,int> State = e.UserState as Tuple<int,int>;

            if (State == null)
            {
                return;
            }

            GenTSPErrorMessage = State.Item1 + " Tested, " + State.Item2 + " Found.";

            GenTSPShownTriSquarePentagon = GenTSPTriSquarePentagon.ToString("0,0");
            GenTSPShownTriangle = GenTSPTriangle.ToString("0,0");
            GenTSPShownSquare = GenTSPSquare.ToString("0,0");
            GenTSPShownPentagon = GenTSPPentagon.ToString("0,0");
        }

        private void BGWorker4_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            GenSQPEPrev2 = 1;
            GenSQPEPrev1 = 9801;

            using (StreamWriter ResultsWriter = File.CreateText(@FileLocation + "TriSquarePentagonResults.dat"))
            {
                ResultsWriter.WriteLine("Number,Pentagon,Square,Triangle");
                ResultsWriter.WriteLine("0,0,0,0");
                ResultsWriter.WriteLine("1,1,1,1");
            }

            int i = 3; //We already "found" 0,0,0 and we incremenet after, so the first find is number 2.
            int Found = 0;
            while (true)
            {

                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    //9602*a(n-1) - a(n-2) + 200
                    try
                    { //Hyper large numbers may cause weirdness? Best to catch it safely

                        GenSQPE = (9602 * GenSQPEPrev1) - GenSQPEPrev2 + 200;

                        BigInteger ThrowAway = BigInteger.Zero;
                        if(IsPerfectSquareBS((8 * GenSQPE + 1), ref ThrowAway))
                        {
                            GenTSPTriSquarePentagon = GenSQPE;
                            GenTSPTriangle = TriRtN(GenTSPTriSquarePentagon);
                            GenTSPSquare = SqRtN(GenTSPTriSquarePentagon);
                            GenTSPPentagon = PentRtN(GenTSPTriSquarePentagon);

                            using (StreamWriter ResultsFile = File.AppendText(@FileLocation + "TriSquarePentagonResults.dat"))
                            {
                                ResultsFile.WriteLine(GenTSPTriSquarePentagon + "," + GenTSPPentagon + "," + GenTSPSquare + "," + GenTSPTriangle);
                            }

                            Found++;
                        }

                        GenSQPEPrev2 = GenSQPEPrev1;
                        GenSQPEPrev1 = GenSQPE;
                    }
                    catch (Exception err)
                    {
                        e.Cancel = true;
                        System.Windows.Forms.MessageBox.Show("Unknown Error Caused: \n" + err.Message);
                        break;
                    }
                }

                if (i % 100 == 0)
                {
                    Worker.ReportProgress(0, new Tuple<int,int>(i, Found));
                    using (StreamWriter DropCatcher = File.CreateText(@FileLocation + "DropCatcher.dat"))
                    {
                        DropCatcher.WriteLine(i);
                        DropCatcher.WriteLine("GenSQPEPrev1: " + GenSQPEPrev1);
                        DropCatcher.WriteLine("GenSQPEPrev2: " + GenSQPEPrev2);
                    }
                }
                i++;
            }
        }

        private void BGWorker3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WorkerRunning = false;
            WorkerStopped = true;
            PenErrorMessage = "COMPLETE!";
        }

        private void BGWorker3_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Tuple<BigInteger, BigInteger, BigInteger, BigInteger> State = e.UserState as Tuple<BigInteger, BigInteger, BigInteger, BigInteger>;

            PenErrorMessage = "Latest TriSquarePentagon: " + State.Item1.ToString("0,0") + "\nPentagon: " + State.Item4.ToString("0,0") + "\nSquare: " + State.Item2.ToString("0,0") + "\nTriangle: " + State.Item3.ToString("0,0") + ".";
        }

        private void BGWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            Square = StartSquare;
            Triangle = StartTriangle;
            Pentagon = StartPentagonal;

            if (NewFile)
            {
                using (StreamWriter ResultsWriter = File.CreateText(@FileLocation + "PentagonalResults.dat"))
                {
                    ResultsWriter.WriteLine("Number,Square,Triangle,Pentagon");
                }
            }
            else
            {
                if (!File.Exists(@FileLocation + "PentagonalResults.dat"))
                {
                    PenErrorMessage = "PentagonalResults.dat does not exist! Check Create New file to create one.";
                    return;
                }
            }

            int i = 0;
            while (true)
            {
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    BigInteger SQ = Squared;
                    BigInteger TR = Triangled;
                    BigInteger PE = Pentagoned;
                    if (SQ == TR && SQ == PE && TR == PE)
                    {
                        //MATCH FOUND
                        using (StreamWriter ResultsFile = File.AppendText(@FileLocation + "PentagonalResults.dat"))
                        {
                            ResultsFile.WriteLine(Squared + "," + Square + "," + Triangle + "," + Pentagon);
                        }
                        Worker.ReportProgress(Convert.ToInt32(0), new Tuple<BigInteger, BigInteger, BigInteger, BigInteger>(Squared, Square, Triangle, Pentagon));
                        Square++;
                        Triangle++;
                        Pentagon++;
                    }
                    else
                    {
                        BigInteger Min = BigInteger.Min(SQ, BigInteger.Min(TR, PE));

                        if (SQ == Min)
                            Square++;
                        if (TR == Min)
                            Triangle++;
                        if (PE == Min)
                            Pentagon++;
                    }
                }

                if (i % 10000 == 0)
                {
                    PenStatusMessage = "Current Square: " + Square.ToString("0,0") + "\nCurrent Triangle: " + Triangle.ToString("0,0") + "\nCurrent Pentagon: " + Pentagon.ToString("0,0") + ".";

                    if(i % 1000000 == 0)
                    {
                        using (StreamWriter DropCatcher = File.CreateText(@FileLocation + "DropCatcher.dat"))
                        {
                            DropCatcher.WriteLine(i);
                            DropCatcher.WriteLine("Triangle: " + Triangle);
                            DropCatcher.WriteLine("Square: " + Square);
                            DropCatcher.WriteLine("Pentagon: " + Pentagon);
                        }
                    }
                }

                i++;
            }
        }

        private void BGWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WorkerRunning = false;
            WorkerStopped = true;
            GenErrorMessage = "COMPLETE!";
        }

        private void BGWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int? TrisFound = e.UserState as int?;

            if(TrisFound != null)
            {
                GenFound = Convert.ToInt32(TrisFound).ToString("0,0");
            }

            GenShownTriSquare = GenTriSquare.ToString("0,0");
            GenShownTriangle = GenTriangle.ToString("0,0");
            GenShownSquare = GenSquare.ToString("0,0");
        }

        private void BGWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            GenTriSquare = 1;
            
            using (StreamWriter ResultsWriter = File.CreateText(@FileLocation + "GenResults.dat"))
            {
                ResultsWriter.WriteLine("Number,Square,Triangle");
                ResultsWriter.WriteLine("0,0,0");
            }

            int i = 2; //We already "found" 0,0,0 and we incremenet after, so the first find is number 2.
            while (true)
            {

                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    //1 + 17*L + 6*sqrt(L + 8*L^2)
                    try
                    { //Hyper large numbers may cause weirdness? Best to catch it safely
                        BigInteger NextTriSquare = (1 + (17 * GenTriSquare) + (6 * SqRtN(GenTriSquare + (8 * (GenTriSquare * GenTriSquare)))));

                        GenSquare = SqRtN(NextTriSquare);
                        GenTriangle = TriRtN(NextTriSquare);
                        GenTriSquare = NextTriSquare;

                        using (StreamWriter ResultsFile = File.AppendText(@FileLocation + "GenResults.dat"))
                        {
                            ResultsFile.WriteLine(GenTriSquare + "," + GenSquare + "," + GenTriangle);
                        }
                    }
                    catch(Exception err)
                    {
                        e.Cancel = true;
                        System.Windows.Forms.MessageBox.Show("Unknown Error Caused: \n" + err.Message);
                        break;
                    }
                }

                if(i % 100 == 0)
                {
                    Worker.ReportProgress(0, i);
                }
                i++;
            }
        }

        private BigInteger SqRtN(BigInteger N)
        {
            /*++
             *  Using Newton Raphson method we calculate the
             *  square root (N/g + g)/2
             */
            BigInteger rootN = N;
            int count = 0;
            int bitLength = 1; // There is a bug in finding bit length hence we start with 1 not 0
            while (rootN / 2 != 0)
            {
                rootN /= 2;
                bitLength++;
            }
            bitLength = (bitLength + 1) / 2;
            rootN = N >> bitLength;

            BigInteger lastRoot = BigInteger.Zero;
            do
            {
                if (lastRoot > rootN)
                {
                    if (count++ > 1000)                   // Work around for the bug where it gets into an infinite loop
                    {
                        return rootN;
                    }
                }
                lastRoot = rootN;
                rootN = (BigInteger.Divide(N, rootN) + rootN) >> 1;
            }
            while (!((rootN ^ lastRoot).ToString() == "0"));
            return rootN;
        } // SqRtN

        private BigInteger TriRtN(BigInteger N)
        {
            //(rt(8x+1)-1)/2
            BigInteger Ret = ((SqRtN(8 * N + 1) - 1) / 2);

            return Ret;
        }

        private BigInteger PentRtN(BigInteger N)
        {
            //(sqrt(24x+1)+1)/6
            BigInteger Ret = ((SqRtN(24 * N + 1) + 1) / 6);

            return Ret;
        }

        private void BGWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WorkerRunning = false;
            WorkerStopped = true;
            ErrorMessage = "COMPLETE!";
        }

        private void BGWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Tuple<BigInteger, BigInteger, BigInteger> State = e.UserState as Tuple<BigInteger, BigInteger, BigInteger>;

            ErrorMessage = "Latest TriSquare: " + State.Item1.ToString("0,0") + ", using Square of: " + State.Item2.ToString("0,0") + ", and Triangle of: " + State.Item3.ToString("0,0") + ".";
        }

        private void BGWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            Square = StartSquare;
            Triangle = StartTriangle;

            if(NewFile)
            {
                using (StreamWriter ResultsWriter = File.CreateText(@FileLocation + "Results.dat"))
                {
                    ResultsWriter.WriteLine("Number,Square,Triangle");
                }
            }
            else
            {
                if(!File.Exists(@FileLocation + "Results.dat"))
                {
                    ErrorMessage = "Results.dat does not exist! Check Create New file to create one.";
                    return;
                }
            }

            int i = 0;
            while (true)
            {
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    BigInteger SQ = Squared;
                    BigInteger TR = Triangled;

                    if (SQ == TR)
                    {
                        //MATCH FOUND
                        using (StreamWriter ResultsFile = File.AppendText(@FileLocation + "Results.dat"))
                        {
                            ResultsFile.WriteLine(Squared + "," + Square + "," + Triangle);
                        }
                        Worker.ReportProgress(Convert.ToInt32(0), new Tuple<BigInteger, BigInteger, BigInteger>(Squared, Square, Triangle));
                        Square++;
                        Triangle++;
                    }
                    else if (SQ < TR)
                    {
                        Square++;
                    }
                    else
                    {
                        Triangle++;
                    }
                }

                if (i % 10000 == 0)
                {
                    StatusMessage = "Current Square: " + Square.ToString("0,0") + ", Current Triangle: " + Triangle.ToString("0,0") + ".";

                    if (i % 1000000 == 0)
                    {
                        using (StreamWriter DropCatcher = File.CreateText(@FileLocation + "DropCatcher.dat"))
                        {
                            DropCatcher.WriteLine(i);
                            DropCatcher.WriteLine("Triangle: " + Triangle);
                            DropCatcher.WriteLine("Square: " + Square);
                        }
                    }
                }

                i++;
            }
        }

        private BigInteger Squared
        {
            get
            {
                return Square * Square;
            }
        }

        private BigInteger Triangled
        {
            get
            {
                return ((Triangle * (Triangle + 1)) / 2);
            }
        }

        private BigInteger Pentagoned
        {
            get
            {
                //(3n^2-n)/2
                return (((3 * (Pentagon * Pentagon)) - Pentagon) / 2);
            }
        }

        private bool IsPerfectSquareBS(BigInteger n, ref BigInteger sqr)
        {
            sqr = -1;
            if (n < 0)
            {
                return false;
            }
            else if (n == 0 || n == 1)
            {
                sqr = n;
                return true;
            }
            //Find the lower and upper limits
            BigInteger lower = 1;
            BigInteger upper = 2;
            while (n < (lower * lower) || n > (upper * upper))
            {
                lower = upper;
                upper *= upper;
            }
            //Binary search
            while (lower + 1 < upper)
            {
                BigInteger mid = (lower + upper) / 2;
                BigInteger midSq = mid * mid;
                if (midSq == n)
                {
                    sqr = mid;
                    return true;
                }
                else if (midSq > n)
                {
                    upper = mid;
                }
                else
                {
                    lower = mid;
                }
            }
            return false;
        }

        private ICommand calculate;
        public ICommand Calculate
        {
            get
            {
                if (calculate == null)
                {
                    calculate = new RelayCommand(CalculateEx, null);
                }
                return calculate;
            }
        }
        private void CalculateEx(object p)
        {
            string LastChar = FileLocation.Substring(FileLocation.Length - 1);
            if (LastChar != "/" && LastChar != "\\")
            {
                FileLocation += "\\";
            }

            try
            {
                Directory.CreateDirectory(FileLocation);
            }
            catch
            {
                ErrorMessage = "Unable to access the given location!";
                return;
            }

            WorkerRunning = true;
            WorkerStopped = false;

            BGWorker.RunWorkerAsync();
        }
        
        private ICommand cancel;
        public ICommand Cancel
        {
            get
            {
                if (cancel == null)
                {
                    cancel = new RelayCommand(CancelEx, null);
                }
                return cancel;
            }
        }
        private void CancelEx(object p)
        {
            if (BGWorker.WorkerSupportsCancellation && BGWorker.IsBusy)
            {
                BGWorker.CancelAsync();
            }
            if (BGWorker2.WorkerSupportsCancellation && BGWorker2.IsBusy)
            {
                BGWorker2.CancelAsync();
            }
            if (BGWorker3.WorkerSupportsCancellation && BGWorker3.IsBusy)
            {
                BGWorker3.CancelAsync();
            }
            if (BGWorker4.WorkerSupportsCancellation && BGWorker4.IsBusy)
            {
                BGWorker4.CancelAsync();
            }
            if (BGWorker5.WorkerSupportsCancellation && BGWorker5.IsBusy)
            {
                BGWorker5.CancelAsync();
            }
            if (BGWorker6.WorkerSupportsCancellation && BGWorker6.IsBusy)
            {
                BGWorker6.CancelAsync();
            }
        }
        
        private ICommand genCalculate;
        public ICommand GenCalculate
        {
            get
            {
                if (genCalculate == null)
                {
                    genCalculate = new RelayCommand(GenCalculateEx, null);
                }
                return genCalculate;
            }
        }
        private void GenCalculateEx(object p)
        {
            string LastChar = FileLocation.Substring(FileLocation.Length - 1);
            if (LastChar != "/" && LastChar != "\\")
            {
                FileLocation += "\\";
            }

            try
            {
                Directory.CreateDirectory(FileLocation);
            }
            catch
            {
                GenErrorMessage = "Unable to access the given location!";
                return;
            }

            WorkerRunning = true;
            WorkerStopped = false;
            GenErrorMessage = "Running";

            BGWorker2.RunWorkerAsync();
        }

        private ICommand penCalculate;
        public ICommand PenCalculate
        {
            get
            {
                if (penCalculate == null)
                {
                    penCalculate = new RelayCommand(PenCalculateEx, null);
                }
                return penCalculate;
            }
        }
        private void PenCalculateEx(object p)
        {
            string LastChar = FileLocation.Substring(FileLocation.Length - 1);
            if (LastChar != "/" && LastChar != "\\")
            {
                FileLocation += "\\";
            }

            try
            {
                Directory.CreateDirectory(FileLocation);
            }
            catch
            {
                PenErrorMessage = "Unable to access the given location!";
                return;
            }

            WorkerRunning = true;
            WorkerStopped = false;

            BGWorker3.RunWorkerAsync();
        }

        private ICommand genTSPCalculate;
        public ICommand GenTSPCalculate
        {
            get
            {
                if (genTSPCalculate == null)
                {
                    genTSPCalculate = new RelayCommand(GenTSPCalculateEx, null);
                }
                return genTSPCalculate;
            }
        }
        private void GenTSPCalculateEx(object p)
        {
            string LastChar = FileLocation.Substring(FileLocation.Length - 1);
            if (LastChar != "/" && LastChar != "\\")
            {
                FileLocation += "\\";
            }

            try
            {
                Directory.CreateDirectory(FileLocation);
            }
            catch
            {
                GenErrorMessage = "Unable to access the given location!";
                return;
            }

            WorkerRunning = true;
            WorkerStopped = false;
            GenTSPErrorMessage = "Running";

            BGWorker4.RunWorkerAsync();
        }

        private ICommand intCalculate;
        public ICommand IntCalculate
        {
            get
            {
                if (intCalculate == null)
                {
                    intCalculate = new RelayCommand(IntCalculateEx, null);
                }
                return intCalculate;
            }
        }
        private void IntCalculateEx(object p)
        {
            string LastChar = FileLocation.Substring(FileLocation.Length - 1);
            if (LastChar != "/" && LastChar != "\\")
            {
                FileLocation += "\\";
            }

            try
            {
                Directory.CreateDirectory(FileLocation);
            }
            catch
            {
                IntErrorMessage = "Unable to access the given location!";
                return;
            }

            WorkerRunning = true;
            WorkerStopped = false;
            IntErrorMessage = "Running";

            BGWorker5.RunWorkerAsync();
        }

        private ICommand int2Calculate;
        public ICommand Int2Calculate
        {
            get
            {
                if (int2Calculate == null)
                {
                    int2Calculate = new RelayCommand(Int2CalculateEx, null);
                }
                return int2Calculate;
            }
        }
        private void Int2CalculateEx(object p)
        {
            string LastChar = FileLocation.Substring(FileLocation.Length - 1);
            if (LastChar != "/" && LastChar != "\\")
            {
                FileLocation += "\\";
            }

            try
            {
                Directory.CreateDirectory(FileLocation);
            }
            catch
            {
                Int2ErrorMessage = "Unable to access the given location!";
                return;
            }

            WorkerRunning = true;
            WorkerStopped = false;
            Int2ErrorMessage = "Running";

            BGWorker6.RunWorkerAsync();
        }

        #region BOOT
        private void SettingsUpgrade()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }
        #endregion

        #region PROPERTIES
        #region MAIN PROPERTIES
        private long startSquare;
        public long StartSquare
        {
            get
            {
                return startSquare;
            }
            set
            {
                startSquare = value;
                OnPropertyChanged("StartSquare");
            }
        }

        private long startTriangle;
        public long StartTriangle
        {
            get
            {
                return startTriangle;
            }
            set
            {
                startTriangle = value;
                OnPropertyChanged("StartTriangle");
            }
        }

        private BigInteger square;
        public BigInteger Square
        {
            get
            {
                return square;
            }
            set
            {
                square = value;
                OnPropertyChanged("Square");
            }
        }

        private BigInteger triangle;
        public BigInteger Triangle
        {
            get
            {
                return triangle;
            }
            set
            {
                triangle = value;
                OnPropertyChanged("Triangle");
            }
        }

        private string fileLocation;
        public string FileLocation
        {
            get
            {
                return fileLocation;
            }
            set
            {
                fileLocation = value;
                Properties.Settings.Default.LastLocation = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("FileLocation");
            }
        }

        private bool workerRunning = false;
        public bool WorkerRunning
        {
            get
            {
                return workerRunning;
            }
            set
            {
                workerRunning = value;
                OnPropertyChanged("WorkerRunning");
            }
        }

        private bool workerStopped = true;
        public bool WorkerStopped
        {
            get
            {
                return workerStopped;
            }
            set
            {
                workerStopped = value;
                OnPropertyChanged("WorkerStopped");
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }

        private string statusMessage;
        public string StatusMessage
        {
            get
            {
                return statusMessage;
            }
            set
            {
                statusMessage = value;
                OnPropertyChanged("StatusMessage");
            }
        }

        private bool newFile = true;
        public bool NewFile
        {
            get
            {
                return newFile;
            }
            set
            {
                newFile = value;
                OnPropertyChanged("NewFile");
            }
        }

        BackgroundWorker BGWorker;
        #endregion

        #region GEN PROPERTIES
        private BigInteger genTriSquare;
        public BigInteger GenTriSquare
        {
            get
            {
                return genTriSquare;
            }
            set
            {
                genTriSquare = value;
                OnPropertyChanged("GenTriSquare");
            }
        }

        private BigInteger genTriangle;
        public BigInteger GenTriangle
        {
            get
            {
                return genTriangle;
            }
            set
            {
                genTriangle = value;
                OnPropertyChanged("GenTriangle");
            }
        }
        
        private BigInteger genSquare;
        public BigInteger GenSquare
        {
            get
            {
                return genSquare;
            }
            set
            {
                genSquare = value;
                OnPropertyChanged("GenSquare");
            }
        }
        
        private string genErrorMessage;
        public string GenErrorMessage
        {
            get
            {
                return genErrorMessage;
            }
            set
            {
                genErrorMessage = value;
                OnPropertyChanged("GenErrorMessage");
            }
        }

        private string genFound;
        public string GenFound
        {
            get
            {
                return genFound;
            }
            set
            {
                genFound = value;
                OnPropertyChanged("GenFound");
            }
        }

        private string genShownTriSquare;
        public string GenShownTriSquare
        {
            get
            {
                return genShownTriSquare;
            }
            set
            {
                genShownTriSquare = value;
                OnPropertyChanged("GenShownTriSquare");
            }
        }

        private string genShownTriangle;
        public string GenShownTriangle
        {
            get
            {
                return genShownTriangle;
            }
            set
            {
                genShownTriangle = value;
                OnPropertyChanged("GenShownTriangle");
            }
        }

        private string genShownSquare;
        public string GenShownSquare
        {
            get
            {
                return genShownSquare;
            }
            set
            {
                genShownSquare = value;
                OnPropertyChanged("GenShownSquare");
            }
        }

        BackgroundWorker BGWorker2;
        #endregion

        #region PEN PROPERTIES
        private long startPentagonal;
        public long StartPentagonal
        {
            get
            {
                return startPentagonal;
            }
            set
            {
                startPentagonal = value;
                OnPropertyChanged("StartPentagonal");
            }
        }
        
        private BigInteger pentagon;
        public BigInteger Pentagon
        {
            get
            {
                return pentagon;
            }
            set
            {
                pentagon = value;
                OnPropertyChanged("Pentagon");
            }
        }

        private string penErrorMessage;
        public string PenErrorMessage
        {
            get
            {
                return penErrorMessage;
            }
            set
            {
                penErrorMessage = value;
                OnPropertyChanged("PenErrorMessage");
            }
        }

        private string penStatusMessage;
        public string PenStatusMessage
        {
            get
            {
                return penStatusMessage;
            }
            set
            {
                penStatusMessage = value;
                OnPropertyChanged("PenStatusMessage");
            }
        }

        BackgroundWorker BGWorker3;
        #endregion

        #region PEN GEN PROPERTIES
        private string genTSPFound;
        public string GenTSPFound
        {
            get
            {
                return genTSPFound;
            }
            set
            {
                genTSPFound = value;
                OnPropertyChanged("GenTSPFound");
            }
        }

        private string genTSPShownTriSquarePentagon;
        public string GenTSPShownTriSquarePentagon
        {
            get
            {
                return genTSPShownTriSquarePentagon;
            }
            set
            {
                genTSPShownTriSquarePentagon = value;
                OnPropertyChanged("GenTSPShownTriSquarePentagon");
            }
        }

        private string genTSPShownTriangle;
        public string GenTSPShownTriangle
        {
            get
            {
                return genTSPShownTriangle;
            }
            set
            {
                genTSPShownTriangle = value;
                OnPropertyChanged("GenTSPShownTriangle");
            }
        }

        private string genTSPShownSquare;
        public string GenTSPShownSquare
        {
            get
            {
                return genTSPShownSquare;
            }
            set
            {
                genTSPShownSquare = value;
                OnPropertyChanged("GenTSPShownSquare");
            }
        }

        private string genTSPShownPentagon;
        public string GenTSPShownPentagon
        {
            get
            {
                return genTSPShownPentagon;
            }
            set
            {
                genTSPShownPentagon = value;
                OnPropertyChanged("GenTSPShownPentagon");
            }
        }

        private string genTSPErrorMessage;
        public string GenTSPErrorMessage
        {
            get
            {
                return genTSPErrorMessage;
            }
            set
            {
                genTSPErrorMessage = value;
                OnPropertyChanged("GenTSPErrorMessage");
            }
        }
        
        private BigInteger genSQPEPrev2;
        public BigInteger GenSQPEPrev2
        {
            get
            {
                return genSQPEPrev2;
            }
            set
            {
                genSQPEPrev2 = value;
                OnPropertyChanged("GenSQPEPrev2");
            }
        }

        private BigInteger genSQPEPrev1;
        public BigInteger GenSQPEPrev1
        {
            get
            {
                return genSQPEPrev1;
            }
            set
            {
                genSQPEPrev1 = value;
                OnPropertyChanged("GenSQPEPrev1");
            }
        }

        private BigInteger genSQPE;
        public BigInteger GenSQPE
        {
            get
            {
                return genSQPE;
            }
            set
            {
                genSQPE = value;
                OnPropertyChanged("GenSQPE");
            }
        }
        
        private BigInteger genTSPTriSquarePentagon;
        public BigInteger GenTSPTriSquarePentagon
        {
            get
            {
                return genTSPTriSquarePentagon;
            }
            set
            {
                genTSPTriSquarePentagon = value;
                OnPropertyChanged("GenTSPTriSquarePentagon");
            }
        }

        private BigInteger genTSPTriangle;
        public BigInteger GenTSPTriangle
        {
            get
            {
                return genTSPTriangle;
            }
            set
            {
                genTSPTriangle = value;
                OnPropertyChanged("GenTSPTriangle");
            }
        }

        private BigInteger genTSPSquare;
        public BigInteger GenTSPSquare
        {
            get
            {
                return genTSPSquare;
            }
            set
            {
                genTSPSquare = value;
                OnPropertyChanged("GenTSPSquare");
            }
        }

        private BigInteger genTSPPentagon;
        public BigInteger GenTSPPentagon
        {
            get
            {
                return genTSPPentagon;
            }
            set
            {
                genTSPPentagon = value;
                OnPropertyChanged("GenTSPPentagon");
            }
        }

        BackgroundWorker BGWorker4;
        #endregion

        #region INTERESTING NUMBER PROPERTIES
        private int interestingNumberPolygons;
        public int InterestingNumberPolygons
        {
            get
            {
                return interestingNumberPolygons;
            }
            set
            {
                interestingNumberPolygons = value;
                OnPropertyChanged("InterestingNumberPolygons");
            }
        }

        private int interestingNumberCount;
        public int InterestingNumberCount
        {
            get
            {
                return interestingNumberCount;
            }
            set
            {
                interestingNumberCount = value;
                OnPropertyChanged("InterestingNumberCount");
            }
        }

        private BigInteger interestingNumber;
        public BigInteger InterestingNumber
        {
            get
            {
                return interestingNumber;
            }
            set
            {
                interestingNumber = value;
                OnPropertyChanged("InterestingNumber");
            }
        }

        private int intStart;
        public int IntStart
        {
            get
            {
                return intStart;
            }
            set
            {
                intStart = value;
                OnPropertyChanged("IntStart");
            }
        }

        private string intErrorMessage;
        public string IntErrorMessage
        {
            get
            {
                return intErrorMessage;
            }
            set
            {
                intErrorMessage = value;
                OnPropertyChanged("IntErrorMessage");
            }
        }

        private string intStatusMessage;
        public string IntStatusMessage
        {
            get
            {
                return intStatusMessage;
            }
            set
            {
                intStatusMessage = value;
                OnPropertyChanged("IntStatusMessage");
            }
        }

        BackgroundWorker BGWorker5;
        #endregion

        #region INTERESTING NUMBER 2 PROPERTIES
        private int interesting2NumberPolygons;
        public int Interesting2NumberPolygons
        {
            get
            {
                return interesting2NumberPolygons;
            }
            set
            {
                interesting2NumberPolygons = value;
                OnPropertyChanged("Interesting2NumberPolygons");
            }
        }
        
        private BigInteger interesting2Number;
        public BigInteger Interesting2Number
        {
            get
            {
                return interesting2Number;
            }
            set
            {
                interesting2Number = value;
                OnPropertyChanged("Interesting2Number");
            }
        }

        private int int2Start;
        public int Int2Start
        {
            get
            {
                return int2Start;
            }
            set
            {
                int2Start = value;
                OnPropertyChanged("Int2Start");
            }
        }
        
        private string int2ErrorMessage;
        public string Int2ErrorMessage
        {
            get
            {
                return int2ErrorMessage;
            }
            set
            {
                int2ErrorMessage = value;
                OnPropertyChanged("Int2ErrorMessage");
            }
        }

        private string int2StatusMessage;
        public string Int2StatusMessage
        {
            get
            {
                return int2StatusMessage;
            }
            set
            {
                int2StatusMessage = value;
                OnPropertyChanged("Int2StatusMessage");
            }
        }

        private int int2MinimumSignificance = 6;
        public int Int2MinimumSignificance
        {
            get
            {
                return int2MinimumSignificance;
            }
            set
            {
                int2MinimumSignificance = value;
                OnPropertyChanged("Int2MinimumSignificance");
            }
        }

        BackgroundWorker BGWorker6;

        List<BigInteger> CurrentBases = new List<BigInteger>();
        List<BigInteger> CurrentNexts = new List<BigInteger>();
        #endregion

        #region PANELS

        #endregion

        #region SETTINGS
        public string BGImage
        {
            get
            {
                if (Properties.Settings.Default.BackgroundImage == 0)
                {
                    return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".png";
                }
                return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".jpg";
            }
            set
            {
                OnPropertyChanged("BGImage");
            }
        }

        private double bgOpacity;
        public double BGOpacity
        {
            get
            {
                return bgOpacity;
            }
            set
            {
                bgOpacity = value;
                OnPropertyChanged("BGOpacity");
            }
        }
        private void SetBGOpacity()
        {
            switch (Properties.Settings.Default.BackgroundImage)
            {
                case 1:
                    BGOpacity = 0.1;
                    break;
                case 2:
                    BGOpacity = 0.1;
                    break;
                case 3:
                    BGOpacity = 0.3;
                    break;
                case 4:
                    BGOpacity = 0.6;
                    break;
                case 5:
                    BGOpacity = 0.2;
                    break;
                case 6:
                    BGOpacity = 0.6;
                    break;
                case 7:
                    BGOpacity = 0.2;
                    break;
                case 8:
                    BGOpacity = 0.2;
                    break;
                case 9:
                    BGOpacity = 0.1;
                    break;
                case 10:
                    BGOpacity = 0.5;
                    break;
                default:
                    BGOpacity = 0.5;
                    break;
            }
        }
        #endregion
        #region MISC
        private ProgramVersion programVersion = new ProgramVersion();
        public string ProgramVersion
        {
            get
            {
                return programVersion.Version;
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        #region MISC
        private ICommand openPreferences;
        public ICommand OpenPreferences
        {
            get
            {
                if (openPreferences == null)
                {
                    openPreferences = new RelayCommand(OpenPreferencesEx, null);
                }
                return openPreferences;
            }
        }
        private void OpenPreferencesEx(object p)
        {
            PreferencesWindow PrefWindow = new PreferencesWindow();
            PreferencesWindowViewModel PrefWindowViewModel = new PreferencesWindowViewModel();
            PrefWindow.DataContext = PrefWindowViewModel;
            PrefWindow.Owner = p as MainWindow;
            PrefWindow.ShowDialog();

            if (PrefWindowViewModel.Saved)
            {
                if (PrefWindowViewModel.BG0)
                {
                    Properties.Settings.Default.BackgroundImage = 0;
                }
                if (PrefWindowViewModel.BG1)
                {
                    Properties.Settings.Default.BackgroundImage = 1;
                }
                if (PrefWindowViewModel.BG2)
                {
                    Properties.Settings.Default.BackgroundImage = 2;
                }
                if (PrefWindowViewModel.BG3)
                {
                    Properties.Settings.Default.BackgroundImage = 3;
                }
                if (PrefWindowViewModel.BG4)
                {
                    Properties.Settings.Default.BackgroundImage = 4;
                }
                if (PrefWindowViewModel.BG5)
                {
                    Properties.Settings.Default.BackgroundImage = 5;
                }
                if (PrefWindowViewModel.BG6)
                {
                    Properties.Settings.Default.BackgroundImage = 6;
                }
                if (PrefWindowViewModel.BG7)
                {
                    Properties.Settings.Default.BackgroundImage = 7;
                }
                if (PrefWindowViewModel.BG8)
                {
                    Properties.Settings.Default.BackgroundImage = 8;
                }
                if (PrefWindowViewModel.BG9)
                {
                    Properties.Settings.Default.BackgroundImage = 9;
                }
                if (PrefWindowViewModel.BG10)
                {
                    Properties.Settings.Default.BackgroundImage = 10;
                }
                Properties.Settings.Default.Save();
                OnPropertyChanged("BGImage");
                SetBGOpacity();
            }
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
